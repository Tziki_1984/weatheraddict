//
//  WADataManager.swift
//  WeatherAddict
//
//  Created by Sion Sasson on 14/12/2019.
//  Copyright © 2019 Sion Sasson. All rights reserved.
//

import UIKit
import MapKit

extension Notification.Name {
    static let cityAddingFailed: Notification.Name = Notification.Name(rawValue: "com.tzikiapplication.waAddict.failedToAddCity")
    static let cityAdded: Notification.Name = Notification.Name(rawValue: "com.tzikiapplication.waAddict.cityAdded")
    static let cityRemoved: Notification.Name = Notification.Name(rawValue: "com.tzikiapplication.waAddict.cityRemoved")
    static let cityUpdated: Notification.Name = Notification.Name(rawValue: "com.tzikiapplication.waAddict.cityUpdated")
}

class WADataManager: NSObject {

    //MARK: - Public statics
    static let shared: WADataManager = WADataManager()
    
    //MARK: - Public statics
    var savedWeatherParameters: [WAWeatherParameters] = [WAWeatherParameters]()
    
    //MARK: - Initializers
    deinit {
        
    }
    
    private override init() {}
    
    //MARK: - Publics
    func start() {
        if let savedParametersData = UserDefaults.standard.value(forKey: WAConstants.UserDefaults.savedWeatherParametersKey) as? Data,
            let savedParameters = try? PropertyListDecoder().decode([WAWeatherParameters].self, from: savedParametersData) {
            savedWeatherParameters.append(contentsOf: savedParameters)
            savedWeatherParameters.forEach({ $0.refresh() })
        }
    }
    
    func addCity(withCoordinate coordinate: CLLocationCoordinate2D, completion: ((_ error: Error?) -> Void)?) {
        coordinate.getCityName { (error, cityName) in
            guard error == nil, let cityName = cityName else {
                NotificationCenter.default.post(name: .cityAddingFailed, object: nil)
                completion?(error)
                return
            }
            
            self.addCity(withName: cityName, completion: completion)
        }
    }
    
    func addCity(withName name: String, completion: ((_ error: Error?) -> Void)?) {
        WAForecastNetworkRequests.fetchForecast(withCityName: name,
                                                units: self.metricSystem(),
                                                numerOfDaysForecast: WAConstants.forecastDays) { (error, parameters) in
                                                    guard error == nil, let parameters = parameters else {
                                                        NotificationCenter.default.post(name: .cityAddingFailed, object: nil)
                                                        completion?(error)
                                                        return
                                                    }
                                                    
                                                    if parameters.city == nil {
                                                        NotificationCenter.default.post(name: .cityAddingFailed, object: nil)
                                                        completion?(nil)
                                                    }
                                                    else {
                                                        self.addAndSaveParameters(parameters)
                                                        completion?(error)
                                                    }
        }
    }
    
    func toggleMetricSystem() {
        var metric = metricSystem()
        
        switch metric {
        case WAMetricSystem.imperial.rawValue:
            metric = WAMetricSystem.metric.rawValue
        case WAMetricSystem.metric.rawValue:
            metric = WAMetricSystem.imperial.rawValue
        default:
            break
        }
        
        UserDefaults.standard.setValue(metric, forKey: WAConstants.UserDefaults.metricSystemKey)
        savedWeatherParameters.forEach({ $0.refresh() })
    }
    
    func metricSystem() -> String {
        return UserDefaults.standard.string(forKey: WAConstants.UserDefaults.metricSystemKey) ?? WAMetricSystem.metric.rawValue
    }
    
    func clearAllWeatherParameters() {
        savedWeatherParameters.removeAll()
        NotificationCenter.default.post(name: .cityRemoved, object: nil)
        try? UserDefaults.standard.set(PropertyListEncoder().encode(savedWeatherParameters), forKey: WAConstants.UserDefaults.savedWeatherParametersKey)
    }
    
    func removeWeatherParameters(_ parameters: WAWeatherParameters) {
        savedWeatherParameters.removeAll { (params) -> Bool in
            return params.city?.id == parameters.city?.id
        }
        
        NotificationCenter.default.post(name: .cityRemoved, object: parameters)
        try? UserDefaults.standard.set(PropertyListEncoder().encode(savedWeatherParameters), forKey: WAConstants.UserDefaults.savedWeatherParametersKey)
    }
    
    func weatherParametersUpdated(_ updatedParams: WAWeatherParameters?) {
        guard let updatedParams = updatedParams else { return }
        if let index = savedWeatherParameters.firstIndex(where: { (params) -> Bool in
            return params.city?.id == updatedParams.city?.id
        }) {
            savedWeatherParameters.remove(at: index)
            savedWeatherParameters.insert(updatedParams, at: index)
            NotificationCenter.default.post(name: .cityUpdated, object: updatedParams)
            try? UserDefaults.standard.set(PropertyListEncoder().encode(savedWeatherParameters), forKey: WAConstants.UserDefaults.savedWeatherParametersKey)
        }
    }
    
    //MARK: - Private functions
    private func addAndSaveParameters(_ params: WAWeatherParameters) {
        guard savedWeatherParameters.first(where: { $0.city?.id == params.city?.id }) == nil else {
            //We already have this city so don't add it
            return
        }
        
        savedWeatherParameters.append(params)
        NotificationCenter.default.post(name: .cityAdded, object: params)
        try? UserDefaults.standard.set(PropertyListEncoder().encode(savedWeatherParameters), forKey: WAConstants.UserDefaults.savedWeatherParametersKey)
    }
}
