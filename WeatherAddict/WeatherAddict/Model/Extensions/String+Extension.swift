//
//  String+Extension.swift
//  WeatherAddict
//
//  Created by Sion Sasson on 25/12/2019.
//  Copyright © 2019 Sion Sasson. All rights reserved.
//

import UIKit

extension String {
    
    //MARK: - Public statics
    static let placeHolderString: String = "---"
    
    //MARK: - Public statics functions
    static func windDegCode() -> String {
        return "\u{00B0}"
    }
    
    static func degCode() -> String {
        switch WADataManager.shared.metricSystem() {
        case WAMetricSystem.metric.rawValue:
            return "\u{00B0}C"
        default:
            return "\u{00B0}F"
        }
    }
    
    static func speedString() -> String {
        switch WADataManager.shared.metricSystem() {
        case WAMetricSystem.metric.rawValue:
            return "m/s"
        default:
            return "M/h"
        }
    }
}
