//
//  UIView+Extension.swift
//  WeatherAddict
//
//  Created by Sion Sasson on 14/12/2019.
//  Copyright © 2019 Sion Sasson. All rights reserved.
//

import UIKit

extension UIView {
    
    //MARK: - Public static variables
    static let defaultPadding: CGFloat = 20
    static let defaultCellCornerRadius: CGFloat = 10
}
