//
//  UIScreen+Extension.swift
//  WeatherAddict
//
//  Created by Sion Sasson on 14/12/2019.
//  Copyright © 2019 Sion Sasson. All rights reserved.
//

import UIKit

extension UIScreen {
    class var isLargeScreen: Bool {
        return UIScreen.main.bounds.width > 500
    }
}
