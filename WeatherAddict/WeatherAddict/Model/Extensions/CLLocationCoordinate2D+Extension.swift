//
//  CLLocationCoordinate2D+Extension.swift
//  WeatherAddict
//
//  Created by Sion Sasson on 14/12/2019.
//  Copyright © 2019 Sion Sasson. All rights reserved.
//

import UIKit
import MapKit

extension CLLocationCoordinate2D {
    
    //MARK: - Public functions
    func getCityName(completion: ((_ error: Error?, _ cityName: String?) -> Void)?) {
        let geoCoder = CLGeocoder()
        let location = CLLocation(latitude: latitude, longitude: longitude)
        geoCoder.reverseGeocodeLocation(location, completionHandler: { placemarks, error -> Void in
            completion?(error, placemarks?.first?.locality)
        })
    }
}
