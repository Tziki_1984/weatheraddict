//
//  URL+Extension.swift
//  WeatherAddict
//
//  Created by Sion Sasson on 13/12/2019.
//  Copyright © 2019 Sion Sasson. All rights reserved.
//

import UIKit

extension URL {
    /// Add query parameters to url
    /// - Parameter queryItem: the key
    /// - Parameter value: value, if nil it won't be added
    func wa_appendQueryItem(_ queryItem: String, value: Any?) -> URL {
        guard var urlComponents = URLComponents(string: absoluteString), let value = value else {
            return absoluteURL
        }
        
        var queryItems: [URLQueryItem] = urlComponents.queryItems ??  []
        let queryItem = URLQueryItem(name: queryItem, value: "\(value)")
        queryItems.append(queryItem)

        urlComponents.queryItems = queryItems
        return urlComponents.url!
    }
}
