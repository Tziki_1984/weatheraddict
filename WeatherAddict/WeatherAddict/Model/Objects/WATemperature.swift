//
//  WATemperature.swift
//  WeatherAddict
//
//  Created by Sion Sasson on 13/12/2019.
//  Copyright © 2019 Sion Sasson. All rights reserved.
//

import UIKit

struct WATemperature: Hashable, Decodable, Encodable {

    //MARK: - Public variables
    /// Day temperature. Unit Default: Kelvin, Metric: Celsius, Imperial: Fahrenheit.
    var day: Float?
    /// Min daily temperature. Unit Default: Kelvin, Metric: Celsius, Imperial: Fahrenheit.
    var min: Float?
    /// Max daily temperature. Unit Default: Kelvin, Metric: Celsius, Imperial: Fahrenheit.
    var max: Float?
    /// Night temperature. Unit Default: Kelvin, Metric: Celsius, Imperial: Fahrenheit.
    var night: Float?
    /// Evening temperature. Unit Default: Kelvin, Metric: Celsius, Imperial: Fahrenheit.
    var eve: Float?
    ///Morning temperature. Unit Default: Kelvin, Metric: Celsius
    var morn: Float?
}
