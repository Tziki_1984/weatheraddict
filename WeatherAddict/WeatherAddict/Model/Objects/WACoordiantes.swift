//
//  WACoordiantes.swift
//  WeatherAddict
//
//  Created by Sion Sasson on 13/12/2019.
//  Copyright © 2019 Sion Sasson. All rights reserved.
//

import UIKit

struct WACoordiantes: Hashable, Decodable, Encodable {

    //MARK: - Public variables
    /// City geo location, longitude
    var lon: Float?
    /// City geo location, latitude
    var lat: Float?
}
