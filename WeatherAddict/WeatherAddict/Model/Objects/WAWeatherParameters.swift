//
//  WAWeatherParameters.swift
//  WeatherAddict
//
//  Created by Sion Sasson on 13/12/2019.
//  Copyright © 2019 Sion Sasson. All rights reserved.
//

import UIKit

struct WAWeatherParameters: Hashable, Decodable, Encodable {
    
    //MARK: - Private variables
    private let identifier = UUID()
    /// City
    var city: WACity?
    /// Forecast
    var list: [WAForecast]?
    
    //MARK: - Public functions
    func todaysForcast() -> WAForecast? {
        return list?.first
    }
    
    func refresh() {
        guard let cityName = city?.name else { return }
        WAForecastNetworkRequests.fetchForecast(withCityName: cityName,
                                                units: WADataManager.shared.metricSystem(),
                                                numerOfDaysForecast: WAConstants.forecastDays) { (error, parameters) in
                                                    WADataManager.shared.weatherParametersUpdated(parameters)
        }
    }
    
    //MARK: - Making struck hashable
    func hash(into hasher: inout Hasher) {
        hasher.combine(identifier)
    }

    static func ==(lhs: WAWeatherParameters, rhs: WAWeatherParameters) -> Bool {
        return lhs.identifier == rhs.identifier
    }
}
