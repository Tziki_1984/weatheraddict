//
//  WAWeather.swift
//  WeatherAddict
//
//  Created by Sion Sasson on 13/12/2019.
//  Copyright © 2019 Sion Sasson. All rights reserved.
//

import UIKit

enum WAIconType {
    case clearSky
    case fewClouds
    case scatteredClouds
    case brokenClouds
    case showerRain
    case rain
    case thunderstorm
    case snow
    case mist
}

struct WAWeather: Hashable, Decodable, Encodable {

    //MARK: - Public variables
    var iconUrl: URL? {
        guard let icon = icon else { return nil }
        return URL(string: WAConstants.API.iconUrl.replacingOccurrences(of: "{icon}", with: icon))
    }
    
    /// Weather condition id
    var id: Int?
    /// Group of weather parameters (Rain, Snow, Extreme etc.)
    var main: String?
    /// Weather condition within the group
    var weatherDescription: String?
    /// Weather icon id
    var icon: String?
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        main = try container.decodeIfPresent(String.self, forKey: .main)
        id = try container.decodeIfPresent(Int.self, forKey: .id)
        weatherDescription = try container.decodeIfPresent(String.self, forKey: .weatherDescription)
        icon = try container.decodeIfPresent(String.self, forKey: .icon)
    }
    
    private enum CodingKeys : String, CodingKey {
        case id,
        main,
        icon,
        weatherDescription = "description"
    }
}
