//
//  WAForecast.swift
//  WeatherAddict
//
//  Created by Sion Sasson on 13/12/2019.
//  Copyright © 2019 Sion Sasson. All rights reserved.
//

import UIKit

struct WAForecast: Hashable, Decodable, Encodable {

    //MARK: - Public variables
    var dt: TimeInterval?
    var weather: [WAWeather]?
    var temp: WATemperature?
    var main: WAMain?
    var humidity: Float?
    var rain: Float?
    var speed: Float?
    var deg: Float?
    
    //MARK: - Public functions
    func tempRangeString() -> String {
        guard let min = temp?.min, let max = temp?.max else {
            return String.placeHolderString
        }
        
        let roundMin = Int(round(min))
        let roundMax = Int(round(max))
        return roundMin == roundMax ? "\(roundMin)\(String.degCode())" : "\(roundMin)\(String.degCode()) - \(roundMax)\(String.degCode())"
    }
    
    func humidityString() -> String {
        guard let humidity = humidity else {
            return String.placeHolderString
        }
        
        return "Humid: \(Int(round(humidity)))%"
    }
    
    func windString() -> String {
        guard let speed = speed, let deg = deg else {
            return String.placeHolderString
        }
        
        return "Wind: \(Int(round(speed))) \(String.speedString()), \(Int(round(deg)))\(String.windDegCode())"
    }
    
    func rainString() -> String {
        guard let rainChance = rain else {
            return String.placeHolderString
        }
        
        return "Rain: \(Int(round(rainChance)))%"
    }
}
