//
//  WAMain.swift
//  WeatherAddict
//
//  Created by Sion Sasson on 21/12/2019.
//  Copyright © 2019 Sion Sasson. All rights reserved.
//

import UIKit

struct WAMain: Hashable, Decodable, Encodable {

    //MARK: - Public variables
    var temp: Float?
    var humidity: Float?
    var temp_min: Float?
    var temp_max: Float?
}
