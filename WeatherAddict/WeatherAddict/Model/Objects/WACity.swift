//
//  WACity.swift
//  WeatherAddict
//
//  Created by Sion Sasson on 13/12/2019.
//  Copyright © 2019 Sion Sasson. All rights reserved.
//

import UIKit

struct WACity: Hashable, Decodable, Encodable {
    //MARK: - Public variables
    /// City ID
    var id: Int64?
    /// City name
    var name: String?
    /// City geo location
    var coord: WACoordiantes?
}
