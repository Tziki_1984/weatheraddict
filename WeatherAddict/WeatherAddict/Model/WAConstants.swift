//
//  WAConstants.swift
//  WeatherAddict
//
//  Created by Sion Sasson on 13/12/2019.
//  Copyright © 2019 Sion Sasson. All rights reserved.
//

import UIKit

enum WAMetricSystem: String {
    case metric = "metric"
    case imperial = "imperial"
}

struct WAConstants {

    static let forecastDays: Int = 6
    
    struct ErrorCodes {
        static let cityNotFound: Int = 1999
    }
    
    struct UI {
        static let animationTime: TimeInterval = 0.3
    }
    
    struct UserDefaults {
        static let metricSystemKey: String = WAMetricSystem.metric.rawValue
        static let savedWeatherParametersKey: String = "savedWeatherParametersKey"
    }
    
    struct API {
        static let ApiKey: String = "c6e381d8c7ff98f0fee43775817cf6ad"
        static let baseUrl: URL = URL(string: "http://api.openweathermap.org/data/2.5/forecast/daily")!.wa_appendQueryItem("APPID", value: ApiKey)
        static let iconUrl: String = "http://openweathermap.org/img/wn/{icon}@2x.png"
        
        struct Query {
            static let numberOfDaysForcast: String = "cnt"
            static let longitude: String = "long"
            static let latitude: String = "lat"
            static let units: String = "units"
            static let query: String = "q"
        }
    }
}
