//
//  WAForecastNetworkRequests.swift
//  WeatherAddict
//
//  Created by Sion Sasson on 13/12/2019.
//  Copyright © 2019 Sion Sasson. All rights reserved.
//

import UIKit

class WAForecastNetworkRequests: NSObject {

    class func fetchForecast(withCityName name: String,
                             units: String,
                             numerOfDaysForecast: Int,
                             completion: ((_ error: Error?, _ weather: WAWeatherParameters?) -> Void)?) {
        let url: URL = WAConstants.API.baseUrl.wa_appendQueryItem(WAConstants.API.Query.units, value: units).wa_appendQueryItem(WAConstants.API.Query.numberOfDaysForcast, value: numerOfDaysForecast).wa_appendQueryItem(WAConstants.API.Query.query, value: name)
        let activeDataTask = URLSession.shared.dataTask(with: url) { (data, response, error) in
            parseResponse(withError: error, data: data, completion: completion)
        }

        activeDataTask.resume()
    }
    
    class func fetchForecast(withLongitude longitude: Double,
                             latitude: Double,
                             units: String,
                             numerOfDaysForecast: Int,
                             completion: ((_ error: Error?, _ weather: WAWeatherParameters?) -> Void)?) {
        
        let url: URL = WAConstants.API.baseUrl.wa_appendQueryItem(WAConstants.API.Query.longitude, value: longitude).wa_appendQueryItem(WAConstants.API.Query.latitude, value: latitude).wa_appendQueryItem(WAConstants.API.Query.units, value: units).wa_appendQueryItem(WAConstants.API.Query.numberOfDaysForcast, value: numerOfDaysForecast)
        let activeDataTask = URLSession.shared.dataTask(with: url) { (data, response, error) in
            parseResponse(withError: error, data: data, completion: completion)
        }

        activeDataTask.resume()
    }
    
    //MARK: - Private class functions
    private class func parseResponse(withError error: Error?,
                                     data: Data?,
                                     completion: ((_ error: Error?, _ weather: WAWeatherParameters?) -> Void)?) {
        guard error == nil else {
            completion?(error, nil)
            return
        }
        
        guard let data = data else { return }
        do { completion?(nil, try JSONDecoder().decode(WAWeatherParameters.self, from: data)) }
        catch {
            completion?(nil, nil)
        }
    }
}
