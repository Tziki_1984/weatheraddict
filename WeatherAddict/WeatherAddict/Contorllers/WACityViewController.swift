//
//  WACityViewController.swift
//  WeatherAddict
//
//  Created by Sion Sasson on 14/12/2019.
//  Copyright © 2019 Sion Sasson. All rights reserved.
//

import UIKit
import Nuke

class WACityViewController: WABaseViewController {

    //MARK: - Publics
    var parameters: WAWeatherParameters? { didSet { refreshData(animated: false) } }
    
    //MARK: - Private variables
    @IBOutlet private weak var emptyStateLabel: UILabel!
    @IBOutlet private weak var controllerCollectionView: UICollectionView!
    @IBOutlet private weak var currentStatsLabel: UILabel!
    @IBOutlet private weak var currentStateLabel: UILabel!
    @IBOutlet private weak var cityNameLabel: UILabel!
    @IBOutlet private weak var todaysDateLabel: UILabel!
    @IBOutlet private weak var todaysTempertureLabel: UILabel!
    @IBOutlet private weak var iconImageView: UIImageView!
    private var dataSource: UICollectionViewDiffableDataSource<Int, WAForecast>! = nil
    
    //MARK: - Overrides
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    override internal func setupUI() {
        super.setupUI()
        navigationController?.navigationBar.tintColor = .systemBackground
        
        NotificationCenter.default.addObserver(self, selector: #selector(cityRemoved(_:)), name: .cityRemoved, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(dataChanged(_:)), name: .cityUpdated, object: nil)
        
        controllerCollectionView.collectionViewLayout = createLayout()
        controllerCollectionView.register(UINib(nibName: WACityDataCollectionViewCell.identifier, bundle: nil),
                                          forCellWithReuseIdentifier: WACityDataCollectionViewCell.identifier)
        configureDataSource()
        refreshData(animated: false)
    }
    
    //MARK: - Notification functions
    @objc private func cityRemoved(_ notification: Notification) {
        DispatchQueue.main.async {
            //if the object is nil then all cities were removed
            if notification.object == nil || (notification.object as? WAWeatherParameters)?.city?.id == self.parameters?.city?.id {
                self.parameters = nil
            }
        }
    }
    
    @objc private func dataChanged(_ notification: Notification) {
        DispatchQueue.main.async {
            if (notification.object as? WAWeatherParameters)?.city?.id == self.parameters?.city?.id {
                self.parameters = notification.object as? WAWeatherParameters
            }
        }
    }
    
    //MARK: - Private functions
    private func configureDataSource() {
        dataSource = UICollectionViewDiffableDataSource<Int, WAForecast>(collectionView: controllerCollectionView, cellProvider: { (collectionView, indexPath, forecast) -> UICollectionViewCell? in
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: WACityDataCollectionViewCell.identifier,
                                                          for: indexPath) as! WACityDataCollectionViewCell
            cell.forecast = forecast
            return cell
        })
    }
    
    private func refreshData(animated: Bool) {
        guard isViewLoaded else { return }
        
        guard parameters != nil else {
            title = nil
            todaysDateLabel.text = nil
            cityNameLabel.text = nil
            todaysTempertureLabel.text = nil
            currentStateLabel.text = nil
            currentStatsLabel.text = nil
            iconImageView.image = nil
            let currentSnapshot = NSDiffableDataSourceSnapshot<Int, WAForecast>()
            dataSource.apply(currentSnapshot, animatingDifferences: animated, completion: nil)
            UIView.animate(withDuration: WAConstants.UI.animationTime) { self.emptyStateLabel.alpha = 1 }
            return
        }
        
        UIView.animate(withDuration: WAConstants.UI.animationTime) { self.emptyStateLabel.alpha = 0 }
        title = parameters?.city?.name
        
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE, MMMM dd"
        todaysDateLabel.text = dateFormatter.string(from: Date())
        cityNameLabel.text = parameters?.city?.name
        
        if let todaysForcast: WAForecast = parameters?.todaysForcast() {
            todaysTempertureLabel.text = todaysForcast.tempRangeString()
            currentStateLabel.text = todaysForcast.weather?.first?.weatherDescription
            currentStatsLabel.text = "\(todaysForcast.humidityString())\n\(todaysForcast.windString())\n\(todaysForcast.rainString())"
            if let url: URL = todaysForcast.weather?.first?.iconUrl {
                Nuke.loadImage(with: url, into: iconImageView)
            }
        }
        
        guard var list = parameters?.list else { return }

        list = Array(list.dropFirst())
        var currentSnapshot = NSDiffableDataSourceSnapshot<Int, WAForecast>()
        currentSnapshot.appendSections([0])
        currentSnapshot.appendItems(list, toSection: 0)
        dataSource.apply(currentSnapshot, animatingDifferences: animated, completion: nil)
    }
}

//MARK: - CollectionView layout
extension WACityViewController {
    private func group() -> NSCollectionLayoutGroup {
        let itemSize: NSCollectionLayoutSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1),
                                                                      heightDimension: .fractionalHeight(1))
        let item = NSCollectionLayoutItem(layoutSize: itemSize)
        let items: [NSCollectionLayoutItem] = [item]
        let group = NSCollectionLayoutGroup.horizontal(layoutSize: NSCollectionLayoutSize(widthDimension: .absolute(130),
                                                                                          heightDimension: .fractionalHeight(1)),
                                                       subitems: items)
        return group
    }

    func createLayout() -> UICollectionViewLayout {
        let sectionProvider = { (sectionIndex: Int, layoutEnvironment: NSCollectionLayoutEnvironment) -> NSCollectionLayoutSection? in
            let section = NSCollectionLayoutSection(group: self.group())
            section.contentInsets = NSDirectionalEdgeInsets(top: UIView.defaultPadding, leading: UIView.defaultPadding, bottom: UIView.defaultPadding, trailing: UIView.defaultPadding)
            section.interGroupSpacing = UIView.defaultPadding
            return section
        }

        let config = UICollectionViewCompositionalLayoutConfiguration()
        config.scrollDirection = .horizontal
        let layout = UICollectionViewCompositionalLayout(sectionProvider: sectionProvider,
                                                         configuration: config)
        return layout
    }
}
