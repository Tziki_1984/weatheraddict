//
//  WASettingsViewController.swift
//  WeatherAddict
//
//  Created by Sion Sasson on 25/12/2019.
//  Copyright © 2019 Sion Sasson. All rights reserved.
//

import UIKit

class WASettingsViewController: WABaseViewController {

    //MARK: - Private variables
    @IBOutlet weak var metricLabel: UILabel!
    
    //MARK: - Overrides
    override func viewDidLoad() {
        super.viewDidLoad()
        updateMetricLabel()
    }
    
    //MARK: - Actions
    @IBAction func clearTapped(_ sender: Any) {
        WADataManager.shared.clearAllWeatherParameters()
    }
    
    @IBAction func toggleTapped(_ sender: UIButton) {
        WADataManager.shared.toggleMetricSystem()
        updateMetricLabel()
    }
    
    //MARK: - Private functions
    private func updateMetricLabel() {
        metricLabel.text = "Metric / Imperial (current \(WADataManager.shared.metricSystem()))"
    }
}
