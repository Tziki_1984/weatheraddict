//
//  WABaseViewController.swift
//  WeatherAddict
//
//  Created by Sion Sasson on 22/12/2019.
//  Copyright © 2019 Sion Sasson. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class WABaseViewController: UIViewController {

    //MARK: - Privagte constants
    private static let loaderSize: CGSize = CGSize(width: 60, height: 60)
    
    //MARK: - Internal variables
    internal var loadingView: NVActivityIndicatorView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    //MARK: - Internal functions
    internal func setupUI() {
        let layer: CAGradientLayer = view.layer as! CAGradientLayer
        layer.startPoint = CGPoint(x: 0, y: 0)
        layer.endPoint = CGPoint(x: 1, y: 1)
        layer.colors = [UIColor.systemIndigo.withAlphaComponent(0.5).cgColor, UIColor.systemIndigo.cgColor]
    }
    
    internal func showLoading() {
        DispatchQueue.main.async {
            guard self.loadingView == nil else { return }
            
            let frame = CGRect(x: (self.view.frame.width - WABaseViewController.loaderSize.width) / 2,
                               y: (self.view.frame.height - WABaseViewController.loaderSize.height) / 2,
                               width: WABaseViewController.loaderSize.width,
                               height: WABaseViewController.loaderSize.height)
            self.loadingView = NVActivityIndicatorView(frame: frame, type: .orbit, color: .systemIndigo, padding: nil)
            self.view.addSubview(self.loadingView!)
            self.loadingView?.startAnimating()
        }
    }
    
    internal func hideLoading() {
        DispatchQueue.main.async {
            self.loadingView?.stopAnimating()
            self.loadingView?.removeFromSuperview()
            self.loadingView = nil
        }
    }
}

//MARK: - NVActivityIndicatorViewable
extension WABaseViewController: NVActivityIndicatorViewable {
}
