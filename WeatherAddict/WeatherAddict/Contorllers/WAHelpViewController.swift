//
//  WAHelpViewController.swift
//  WeatherAddict
//
//  Created by Sion Sasson on 25/12/2019.
//  Copyright © 2019 Sion Sasson. All rights reserved.
//

import UIKit
import WebKit

class WAHelpViewController: UIViewController {
    
    //MARK: - Private variables
    @IBOutlet private weak var controllerWebView: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let html = """
        <html>
        <body>
        <h1>Weather Addicts</h1>
        <h2>General Notes:</h2>
        <p>Well this is what I was able to accomplished in very little time I had- since my partner had a new baby girl Robin&nbsp;<img src="https://html-online.com/editor/tinymce4_6_5/plugins/emoticons/img/smiley-laughing.gif" alt="laughing" />&nbsp;- after a very lond childbirth process.</p>
        <p>Due to lack of time, I would probably could improve some of the views or the model structure - but the current one is good enough for the excercise.</p>
        <p>The main structure is Tab based, and the home + specific city controllers are inside a UISplitViewController so the iPad and large iphone will have automatic support for viewing both controllers at once (taking adventage of the larger screens).</p>
        <p>The only manager in the app is the entity that holds the data (and uses notifications to update the controllers - since there can be more than one controller listening to changes.</p>
        <h2>How To Use:</h2>
        <ul>
        <li>To add a city you can type the name OR drag a pin to the map</li>
        <li>You can issue a name search by tapping on the white rabbit or simply tap on the Done in the keyboard</li>
        <li>You can delete a city by simply tapping on the trash button in the main screen</li>
        <li>By tapping on the city in the home screen you can enter and see the data on that city</li>
        <li>In the settings screen you can change the metric system of the app - this will trigger API calls to refresh the data on each city. Also you can clear all saved cities by tapping on the second option.</li>
        <li>Also during application startup - the app also refresh the data.</li>
        </ul>
        <h2>Gestures &amp; More</h2>
        <ul>
        <li>We have horizontal collection views (using the new iOS 13 API)&nbsp;</li>
        <li>Using the MapKit (with zooming and pan gestures)</li>
        <li>Dragging the pin to the map (also pan gestures)</li>
        </ul>
        <h2>3rd Party Used</h2>
        <ul>
        <li><a href="https://github.com/kean/Nuke"><strong>Nuke</strong></a> - To download and cache image from url</li>
        <li><a href="https://github.com/scalessec/Toast-Swift"><strong>Toast-Swift</strong></a> - Show toast after adding a city - used it due to lack of time</li>
        <li><strong><a href="https://github.com/ninjaprox/NVActivityIndicatorView">VNActivityIndicatorView</a></strong>- to show something nice while loading cities</li>
        </ul>
        </body>
        </html>
        """
        controllerWebView.loadHTMLString(html, baseURL: nil)
        controllerWebView.navigationDelegate = self
    }
}

//MARK: - WKNavigationDelegate
extension WAHelpViewController: WKNavigationDelegate {
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        switch navigationAction.navigationType {
        case .linkActivated:
            if let url = navigationAction.request.url,
                UIApplication.shared.canOpenURL(url) {
                UIApplication.shared.open(url)
                decisionHandler(.cancel)
            } else {
                decisionHandler(.allow)
            }
        default:
            decisionHandler(.allow)
        }
    }
}
