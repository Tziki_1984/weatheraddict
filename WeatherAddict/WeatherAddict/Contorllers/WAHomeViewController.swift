//
//  WAHomeViewController.swift
//  WeatherAddict
//
//  Created by Sion Sasson on 14/12/2019.
//  Copyright © 2019 Sion Sasson. All rights reserved.
//

import UIKit
import MapKit
import Toast

class WAHomeViewController: WABaseViewController {

    //MARK: - Private constants
    private static let collectionViewHeight: CGFloat = 150
    
    //MARK: - Private variables
    @IBOutlet private weak var controllerCollectionViewALHeight: NSLayoutConstraint!
    @IBOutlet private weak var searchCityTextField: UITextField!
    @IBOutlet private weak var controllerCollectionView: UICollectionView!
    @IBOutlet private weak var containerView: UIView!
    @IBOutlet private weak var pinImageView: UIImageView!
    @IBOutlet private weak var mapView: MKMapView!
    private var draggedPinView: UIImageView?
    private var dataSource: UICollectionViewDiffableDataSource<Int, WAWeatherParameters>! = nil
    
    //MARK: - Overrides
    override internal func setupUI() {
        super.setupUI()
        
        NotificationCenter.default.addObserver(self, selector: #selector(dataChanged(_:)), name: .cityUpdated, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(dataChanged(_:)), name: .cityAdded, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(dataChanged(_:)), name: .cityRemoved, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(dataFetchFailed(_:)), name: .cityAddingFailed, object: nil)
        let panGesture: UIPanGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(panGestureRecognized(_:)))
        pinImageView.addGestureRecognizer(panGesture)
        containerView.layer.cornerRadius = containerView.frame.size.height / 2
        
        controllerCollectionView.collectionViewLayout = createLayout()
        controllerCollectionView.register(UINib(nibName: WAHomeCityCollectionViewCell.identifier, bundle: nil),
                                          forCellWithReuseIdentifier: WAHomeCityCollectionViewCell.identifier)
        
        let button = UIButton(type: .custom)
        button.setImage(UIImage(systemName: "hare.fill"), for: .normal)
        button.addTarget(self, action: #selector(searchCity), for: .touchUpInside)
        searchCityTextField.rightView = button
        searchCityTextField.rightViewMode = .whileEditing
        searchCityTextField.attributedPlaceholder = NSAttributedString(string: "Type city name to add", attributes: [NSAttributedString.Key.foregroundColor : UIColor.secondarySystemBackground])
        
        configureDataSource()
        refreshData(animated: false)
        
        refreshCollectionViewHeight(refreshData: false)
    }
    
    //MARK: - Action
    @objc private func searchCity() {
        guard let name = searchCityTextField.text, name.count > 1 else { return }
        
        searchCityTextField.resignFirstResponder()
        showLoading()
        WADataManager.shared.addCity(withName: name) { [weak self] (error) in
            self?.hideLoading()
            DispatchQueue.main.async { if error == nil { self?.searchCityTextField.text = nil } }
        }
    }
    
    //MARK: - Gestures
    @objc private func panGestureRecognized(_ recognizer: UIPanGestureRecognizer) {
        switch recognizer.state {
        case .began:
            addPinImageView()
            draggablePinImageViewStarted()
        case .changed:
            let translation = recognizer.translation(in: view)
            if let draggedPinView = draggedPinView {
                let newPos = CGPoint(x:draggedPinView.center.x + translation.x, y:draggedPinView.center.y + translation.y)
                draggedPinView.center = newPos
                recognizer.setTranslation(.zero, in: view)
            }
        case .ended:
            if let draggedPinView = draggedPinView {
                //get the point of the edge of the pin
                let point = CGPoint(x: draggedPinView.frame.origin.x + draggedPinView.frame.size.width / 2, y: draggedPinView.frame.origin.y + draggedPinView.frame.size.height)
                let coordinate: CLLocationCoordinate2D = mapView.convert(point, toCoordinateFrom: view)
                showLoading()
                WADataManager.shared.addCity(withCoordinate: coordinate) { [weak self] (error) in
                    self?.hideLoading()
                }
            }
            
            hideDraggablePinImageView()
        default:
            hideDraggablePinImageView()
        }
    }
    
    //MARK: - Notification functions
    @objc private func dataChanged(_ notification: Notification) {
        DispatchQueue.main.async {
            switch notification.name {
            case .cityAdded:
                if let city = (notification.object as? WAWeatherParameters)?.city?.name {
                    self.view.makeToast("\(city) was added")
                }
            default:
                break
            }
            
            self.refreshData(animated: true)
            self.refreshCollectionViewHeight(refreshData: true)
        }
    }
    
    @objc private func dataFetchFailed(_ notification: Notification) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Oops", message: "Couldn't find the city you are looking for.. try zooming in to get better percision", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    //MARK: - Private functions
    private func configureDataSource() {
        dataSource = UICollectionViewDiffableDataSource<Int, WAWeatherParameters>(collectionView: controllerCollectionView, cellProvider: { (collectionView, indexPath, parameters) -> UICollectionViewCell? in
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: WAHomeCityCollectionViewCell.identifier,
                                                          for: indexPath) as! WAHomeCityCollectionViewCell
            cell.delegate = self
            cell.parameters = parameters
            return cell
        })
    }
    
    private func addPinImageView() {
        let frame = view.convert(pinImageView.frame, from: containerView)
        draggedPinView = UIImageView(frame: frame)
        draggedPinView?.image = pinImageView.image
        draggedPinView?.tintColor = pinImageView.tintColor
        draggedPinView?.contentMode = pinImageView.contentMode
        view.addSubview(draggedPinView!)
    }
    
    private func draggablePinImageViewStarted() {
        weak var weakSelf = self
        UIView.animate(withDuration: WAConstants.UI.animationTime) {
            weakSelf?.containerView.alpha = 0.3
        }
    }
    
    private func hideDraggablePinImageView() {
        weak var weakSelf = self
        UIView.animate(withDuration: WAConstants.UI.animationTime,
                       animations: {
                        weakSelf?.draggedPinView?.alpha = 0
                        weakSelf?.containerView.alpha = 1
        }) { (done) in
            weakSelf?.draggedPinView?.removeFromSuperview()
            weakSelf?.draggedPinView = nil
        }
    }
    
    private func refreshData(animated: Bool) {
        splitViewController?.viewControllers.forEach({ (controller) in
            if let cityController: WACityViewController = controller as? WACityViewController, cityController.parameters == nil {
                cityController.parameters = WADataManager.shared.savedWeatherParameters.first
            }
        })
        
        var currentSnapshot = NSDiffableDataSourceSnapshot<Int, WAWeatherParameters>()
        currentSnapshot.appendSections([0])
        currentSnapshot.appendItems(WADataManager.shared.savedWeatherParameters, toSection: 0)
        dataSource.apply(currentSnapshot, animatingDifferences: animated, completion: nil)
    }
    
    private func refreshCollectionViewHeight(refreshData: Bool) {
        DispatchQueue.main.async {
            self.controllerCollectionViewALHeight.constant = WADataManager.shared.savedWeatherParameters.count > 0 ? WAHomeViewController.collectionViewHeight : 0
            UIView.animate(withDuration: WAConstants.UI.animationTime, animations: {
                self.view.layoutIfNeeded()
            }) { (finished) in
                if (refreshData) {
                    self.refreshData(animated: true)
                }
            }
        }
    }
}

//MARK: - UICollectionViewDelegate
extension WAHomeViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = storyboard!.instantiateViewController(identifier: "WACityViewController") as! WACityViewController
        vc.parameters = WADataManager.shared.savedWeatherParameters[indexPath.row]
        splitViewController?.showDetailViewController(vc, sender: nil)
    }
}

//MARK: - WAHomeCityCollectionViewCellDelegate
extension WAHomeViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        searchCity()
        return true
    }
}

//MARK: - WAHomeCityCollectionViewCellDelegate
extension WAHomeViewController: WAHomeCityCollectionViewCellDelegate {
    func homeCityCollectionViewCell(_ cell: WAHomeCityCollectionViewCell, deleteTapped button: UIButton) {
        guard let indexPath = controllerCollectionView.indexPath(for: cell) else { return }
        WADataManager.shared.removeWeatherParameters(WADataManager.shared.savedWeatherParameters[indexPath.row])
    }
}

//MARK: - CollectionView layout
extension WAHomeViewController {
    private func group() -> NSCollectionLayoutGroup {
        let itemSize: NSCollectionLayoutSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1),
                                                                      heightDimension: .fractionalHeight(1))
        let item = NSCollectionLayoutItem(layoutSize: itemSize)
        let items: [NSCollectionLayoutItem] = [item]
        
        let group = NSCollectionLayoutGroup.horizontal(layoutSize: NSCollectionLayoutSize(widthDimension: .absolute(150),
                                                                                          heightDimension: .fractionalHeight(1)),
                                                       subitems: items)
        return group
    }

    func createLayout() -> UICollectionViewLayout {
        let sectionProvider = { (sectionIndex: Int, layoutEnvironment: NSCollectionLayoutEnvironment) -> NSCollectionLayoutSection? in
            let section = NSCollectionLayoutSection(group: self.group())
            section.contentInsets = NSDirectionalEdgeInsets(top: UIView.defaultPadding, leading: UIView.defaultPadding, bottom: UIView.defaultPadding, trailing: UIView.defaultPadding)
            section.interGroupSpacing = UIView.defaultPadding
            return section
        }

        let config = UICollectionViewCompositionalLayoutConfiguration()
        config.scrollDirection = .horizontal
        let layout = UICollectionViewCompositionalLayout(sectionProvider: sectionProvider,
                                                         configuration: config)
        return layout
    }
}
