//
//  WASplitViewController.swift
//  WeatherAddict
//
//  Created by Sion Sasson on 22/12/2019.
//  Copyright © 2019 Sion Sasson. All rights reserved.
//

import UIKit

class WASplitViewController: UISplitViewController {

    //MARK: - Public variables
    var selectedParameters: WAWeatherParameters?
    
    //MARK: - Override functions
    override func viewDidLoad() {
        super.viewDidLoad()
        delegate = self
        preferredDisplayMode = .allVisible
    }
}

//MARK: - UISplitViewControllerDelegate
extension WASplitViewController: UISplitViewControllerDelegate {
    func splitViewController(_ splitViewController: UISplitViewController, collapseSecondary secondaryViewController: UIViewController, onto primaryViewController: UIViewController) -> Bool {
        return true
    }
}
