//
//  WATabBarViewController.swift
//  WeatherAddict
//
//  Created by Sion Sasson on 25/12/2019.
//  Copyright © 2019 Sion Sasson. All rights reserved.
//

import UIKit

class WATabBarViewController: UITabBarController {

    //MARK: - Overrides
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    //MARK: - Private function
    private func setupUI() {
    }
}
