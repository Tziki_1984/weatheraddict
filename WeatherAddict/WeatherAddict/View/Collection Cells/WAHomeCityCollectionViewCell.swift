//
//  WAHomeCityCollectionViewCell.swift
//  WeatherAddict
//
//  Created by Sion Sasson on 14/12/2019.
//  Copyright © 2019 Sion Sasson. All rights reserved.
//

import UIKit
import Nuke

protocol WAHomeCityCollectionViewCellDelegate {
    func homeCityCollectionViewCell(_ cell: WAHomeCityCollectionViewCell, deleteTapped button: UIButton)
}

class WAHomeCityCollectionViewCell: WABaseCollectionViewCell {

    //MARK: - Public statics
    static let identifier: String = "WAHomeCityCollectionViewCell"
    
    //MARK: - Public variables
    var delegate: WAHomeCityCollectionViewCellDelegate?
    var parameters: WAWeatherParameters? { didSet { objectDidSet() } }
    
    //MARK: - Private variables
    @IBOutlet private weak var nameLabel: UILabel!
    
    //MARK: - Action functions
    @IBAction func trashTapped(_ sender: UIButton) {
        delegate?.homeCityCollectionViewCell(self, deleteTapped: sender)
    }
    
    //MARK: - Private functions
    private func objectDidSet() {
        nameLabel.text = parameters?.city?.name
        let todaysForcast: WAForecast? = parameters?.todaysForcast()
        applyForcastImage(todaysForcast)
        applyForcastTemperture(todaysForcast)
    }
}
