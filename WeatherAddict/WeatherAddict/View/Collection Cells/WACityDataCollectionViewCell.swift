//
//  WACityDataCollectionViewCell.swift
//  WeatherAddict
//
//  Created by Sion Sasson on 14/12/2019.
//  Copyright © 2019 Sion Sasson. All rights reserved.
//

import UIKit

class WACityDataCollectionViewCell: WABaseCollectionViewCell {
    
    //MARK: - Public statics
    static let identifier: String = "WACityDataCollectionViewCell"
    
    //MARK: - Public variables
    var forecast: WAForecast? { didSet { objectDidSet() } }
    
    //MARK: - Private variables
    @IBOutlet private weak var dateLabel: UILabel!
    
    //MARK: - Private functions
    private func objectDidSet() {
        guard let forecast = forecast else { return }
        
        if let timeInterval = forecast.dt {
            let date: Date = Date(timeIntervalSince1970: timeInterval)
            let dateFormatter: DateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd/MM"
            dateLabel.text = "\(date.dayOfTheWeek())\n\(dateFormatter.string(from: date))"
        }
        
        applyForcastImage(forecast)

        let string: String? = "\(forecast.tempRangeString())\n\(forecast.humidityString())\n\(forecast.windString())\n\(forecast.rainString())"
        dataLabel.text = string
    }
}
