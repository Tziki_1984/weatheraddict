//
//  WABaseCollectionViewCell.swift
//  WeatherAddict
//
//  Created by Sion Sasson on 25/12/2019.
//  Copyright © 2019 Sion Sasson. All rights reserved.
//

import UIKit
import Nuke

class WABaseCollectionViewCell: UICollectionViewCell {
    
    //MARK: - Internal variable
    @IBOutlet private weak var iconImageView: UIImageView?
    @IBOutlet internal weak var dataLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        contentView.backgroundColor = UIColor.systemIndigo.withAlphaComponent(0.3)
        layer.cornerRadius = UIView.defaultCellCornerRadius
        layer.borderWidth = 0.5
        layer.borderColor = UIColor.systemBackground.withAlphaComponent(0.5).cgColor
    }
    
    //MARK: - Internal functions
    internal func applyForcastTemperture(_ forcast: WAForecast?) {
        dataLabel.text = forcast?.tempRangeString()
    }
    
    internal func applyForcastImage(_ forcast: WAForecast?) {
        guard let url: URL = forcast?.weather?.first?.iconUrl, let iconImageView = iconImageView else {
            return
        }
        
        Nuke.loadImage(with: url, into: iconImageView)
    }
}
