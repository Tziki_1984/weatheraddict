//
//  WAGradientLayeredView.swift
//  WeatherAddict
//
//  Created by Sion Sasson on 25/12/2019.
//  Copyright © 2019 Sion Sasson. All rights reserved.
//

import UIKit

class WAGradientLayeredView: UIView {

    override class var layerClass: AnyClass {
        get {
            return CAGradientLayer.self
        }
    }
}
